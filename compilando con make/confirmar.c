#include "header/confirmar.h"
#include<stdio.h>

bool confirmar(struct Usuario usuario){
	printf("Confirme los datos Ingresados:\n");
	printf("\tNombre: %s\n",usuario.nombre);
	printf("\tApellido: %s\n", usuario.apellido );
	printf("\tUsername:  %s\n", usuario.username);
	printf("\tPassword: %s\n",usuario.password );

	char respuesta;
	printf("\t ¿Los datos son correctos? (y/n) = ");
	scanf("\n%c", & respuesta);
		
	
	if(respuesta=='n' || respuesta =='N'){
		return false;
	}
	else{
		return true;
	}
}