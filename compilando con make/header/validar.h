#define MAXSTR 50
#define NO_DIGITS 1
#define NO_LETTER 2

#define SUCCESS 0
#define USERNAME_TOO_LOWER_ERROR 1
#define USERNAME_TOO_LARGE_ERROR 2
#define PASSWORD_TOO_LOWER_ERROR 3
#define NO_LETTER_ERROR 4
#define NO_DIGITS_ERROR 5


struct Usuario{
	char nombre[MAXSTR];
	char apellido[MAXSTR];
	char username[MAXSTR];
	char password[MAXSTR];
	int userid;
};

void userInputData(struct Usuario *);
int validar(struct Usuario *);
int characterCounts(char *);
void firstToUpper(char *);
int generateID(int min, int max);

