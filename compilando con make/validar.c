#include<ctype.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include "header/validar.h"

void userInputData(struct Usuario *usuario){
	printf("\nnombre: ");
	scanf("%s", usuario->nombre);
	printf("\napellido: ");
	scanf("%s", usuario->apellido);
	printf("\nusername: ");
	scanf("%s", usuario->username);
	printf("\npassword: ");
	scanf("%s", usuario->password);
}

int validar(struct Usuario *usuario){  

	if(strlen(usuario-> username) < 8){
		return USERNAME_TOO_LOWER_ERROR;
	}
	else if(strlen(usuario-> username) > 10){
		return USERNAME_TOO_LARGE_ERROR;
	}
	else if(strlen(usuario-> password) < 8){
		return PASSWORD_TOO_LOWER_ERROR;
	}
	else if(characterCounts(usuario -> password) == NO_LETTER){
		return NO_LETTER_ERROR;
	}
	else if(characterCounts(usuario -> password) == NO_DIGITS){
		return NO_DIGITS_ERROR;
	}

	firstToUpper( usuario-> nombre);
	firstToUpper( usuario-> apellido);
	usuario -> userid = generateID(10000, 99999);

	return SUCCESS;
}

int characterCounts(char text[]){
	int countDigit = 0;
	int countChar = 0;
	int i=0;
	while(text[i]){
		if(text[i]>='0' && text[i]<='9')
			countDigit++;
		else
			countChar++;
		i++;
	}

	if(countDigit == 0)
		return NO_DIGITS;
	if(countChar == 0)
		return NO_LETTER;
	return 0;
}

void firstToUpper(char *c){
	c[0] = (char) toupper(c[0]);
}

int generateID(int min, int max){
		int randNumber = min + rand();

}
