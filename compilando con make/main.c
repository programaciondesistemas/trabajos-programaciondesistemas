#include <stdio.h>
#include<stdbool.h>
//#include "validar.h"
#include "header/confirmar.h"


int main() {
	
   struct Usuario usuario;

   userInputData(&usuario);

   int status = 1;
   while(status){

      bool confirma;
      status = validar(&usuario);

   	switch(status){
   		case SUCCESS:
            confirma = confirmar(usuario); 
            if(confirma){
               printf("Usuario creado exitosamente.");
               break; 
            }
            else {
               userInputData(&usuario);
               status = 1;
               break;
            } 
            break;
   		case USERNAME_TOO_LOWER_ERROR:
   			fprintf(stderr ,"\n nombre de usuario muy corto, ingresar nuevamente.");
   			printf("\nusername: ");
   			scanf("%s", usuario.username);
   			break;
   		case USERNAME_TOO_LARGE_ERROR:
   			fprintf(stderr ,"\n nombre de usuario muy largo, ingresar nuevamente.");
   			printf("\nusername: ");
   			scanf("%s", usuario.username);
   			break;
            case PASSWORD_TOO_LOWER_ERROR:
   			fprintf(stderr ,"\n contraseña muy corta, ingresar nuevamente.");
   			printf("\npassword: ");
   			scanf("%s", usuario.password);
   			break;
   		case NO_LETTER_ERROR:
   			fprintf(stderr ,"\n la contraseña debe contener al menos una letra, ingresar nuevamente.");
   			printf("\npassword: ");
   			scanf("%s", usuario.password);
   			break;
   		case NO_DIGITS_ERROR:
   			fprintf(stderr ,"\n la contraseña debe contener al menos un numero, ingresar nuevamente.");
   			printf("\npassword: ");
   			scanf("%s", usuario.password);
   			break;
   	}
   }

   printf("\n%s %s userID: %d \n", usuario.nombre, usuario.apellido, usuario.userid );
   return 0;
}